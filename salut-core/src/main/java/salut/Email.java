package salut;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class Email {
    private NotEmpty email;

    public Email(NotEmpty email) throws ParsingException {
        var pattern = Pattern.compile("^^[A-Za-z0-9._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,63}$");
        Matcher matcher=pattern.matcher(email.inner());
        if(!matcher.find()){
            throw new ParsingException("Email mismatch");
        }
        this.email = email;
    }
    public Email(String email) throws NotNullException, ParsingException {
    	new Email(new NotEmpty(email));
    }
    public String inner(){
        return email.inner();
    }
}
