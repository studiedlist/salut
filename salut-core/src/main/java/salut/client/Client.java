package salut.client;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import salut.NotEmpty;

@Entity
public class Client {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;

	@Column
	private String name;

	@Column
	String email;

	@Column
	String telegram_id;

	@SuppressWarnings("unused")
	private Client() {
	}

	public Client(NotEmpty name) {
		this.name = name.inner();
	}

	public Client(NotEmpty name, String telegram_id, String email) {
		this.name = name.inner();
		this.email = email;
		this.telegram_id = telegram_id;
	}

	public String name() {
		return this.name;
	}
}
