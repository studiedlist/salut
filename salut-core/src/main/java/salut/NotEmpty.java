package salut;

import org.apache.commons.lang3.StringUtils;

public class NotEmpty extends NotNull<String> {

	public NotEmpty(String t) throws NotNullException {
		super(t);
		if (StringUtils.isBlank(t)) {
			throw new NotNullException("Cannot create NotEmpty instance");
		}
	}

}
