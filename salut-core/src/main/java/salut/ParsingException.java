package salut;

public class ParsingException extends Exception {
	private static final long serialVersionUID = 1L;

	public ParsingException(String msg) {
		super(msg);
	}
}
