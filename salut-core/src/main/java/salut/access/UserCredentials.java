package salut.access;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import salut.NotEmpty;
import salut.NotNull;

@Entity
public class UserCredentials {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;

	@Column(unique = true)
	private String login;

	@Column
	private String password;

	@SuppressWarnings("unused")
	private UserCredentials() {
	}

	public UserCredentials(NotNull<UUID> id, NotEmpty login, NotEmpty password) {
		this.id = id.inner();
		this.login = login.inner();
		this.password = password.inner();
	}

	public String password() {
		return this.password;
	}

	public String login() {
		return this.login;
	}

	public UUID id() {
		return this.id;
	}
}
