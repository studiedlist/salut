package salut.feedback;

public class Rating {
	private int rating;

	public Rating(int rating) throws IllegalArgumentException {
		if (this.rating > 10 || this.rating < 0) {
			throw new IllegalArgumentException("Rating must be in range (0..10)");
		}
		this.rating = rating;
	}
}
