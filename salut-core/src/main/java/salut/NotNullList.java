package salut;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

public class NotNullList<T> {

	private List<NotNull<T>> inner;

	public NotNullList() {
		this.inner = new ArrayList<>();
	}

	public NotNullList(NotNull<T> elem) {
		this.inner = new ArrayList<>();
		inner.add(elem);
	}

	public NotNullList(T elem) throws NotNullException {
		this.inner = new ArrayList<>();
		this.inner.add(new NotNull<>(elem));
	}

	public NotNullList(NotNull<T>... elem) {
		this.inner = Arrays.asList(elem);
	}

	public NotNullList(T... elem) throws NotNullException {
		var list = new ArrayList<NotNull<T>>();
		for (var e : elem) {
			list.add(new NotNull<>(e));
		}
		this.inner = list;
	}
	
	public NotNullList(List<T> elem) throws NotNullException {
		var list = new ArrayList<NotNull<T>>();
		for (var e : elem) {
			list.add(NotNull.from(e));
		}
		this.inner = list;
	}

	public int size() {
		return inner.size();
	}

	public boolean isEmpty() {
		return false;
	}

	public boolean contains(Object o) {
		return inner.contains(o);
	}

	public Object[] toArray() {
		return inner.toArray();
	}

	public boolean add(T e) throws NotNullException {
		return inner.add(new NotNull<>(e));
	}

	public boolean add(NotNull<T> e) {
		return inner.add(e);
	}

	public boolean remove(Object o) {
		return inner.remove(o);
	}

	public boolean containsAll(Collection<?> c) {
		return inner.containsAll(c);
	}

	public boolean addAll(Collection<? extends NotNull<T>> c) {
		return inner.addAll(c);
	}
	
	public boolean addAll(NotNullList<T> c) {
		return inner.addAll(c.inner);
	}

	public boolean addAll(int index, Collection<? extends NotNull<T>> c) {
		return inner.addAll(index, c);
	}

	public boolean removeAll(Collection<?> c) {
		return inner.removeAll(c);
	}

	public boolean retainAll(Collection<?> c) {
		return inner.retainAll(c);
	}

	public void clear() {
		inner.clear();
	}

	public NotNull<T> get(int index) {
		return inner.get(index);
	}

	public NotNull<T> set(int index, NotNull<T> element) {
		return inner.set(index, element);
	}

	public void add(int index, NotNull<T> element) {
		inner.add(index, element);
	}

	public NotNull<T> remove(int index) {
		return inner.remove(index);
	}

	public int indexOf(Object o) {
		return inner.indexOf(o);
	}

	public int lastIndexOf(Object o) {
		return inner.lastIndexOf(o);
	}

	public Stream<T> stream() {
		return inner.stream().map(NotNull::inner);
	}

}
