package salut;

public class Telegram {
	private NotEmpty username;
	private Integer id;

	public Telegram(NotEmpty username) throws NotNullException {
		username.map((x) -> {
			if (!x.startsWith("@")) {
				x = "@" + x;
			}
			return x;
		});
		this.username = username;
	}

	public Telegram(String username) throws NotNullException {
		new Telegram(new NotEmpty(username));
	}

	public Telegram(Integer id) {
		this.id = id;
	}

	public boolean isUsername() {
		return username != null;
	}

	public boolean isId() {
		return id != null;
	}

	public NotEmpty getUsername() {
		return username;
	}

	public Integer getId() {
		return id;
	}
}
