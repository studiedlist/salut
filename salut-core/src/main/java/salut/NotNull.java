package salut;

import java.util.function.Function;

public class NotNull<T> {
	private T t;

	public NotNull(T t) throws NotNullException {
		if (t == null) {
			throw new NotNullException("Error creating NotNull instance");
		}
		this.t = t;
	}

	public T inner() {
		return this.t;
	}

	public <R> NotNull<R> map(Function<T, R> func) throws NotNullException {
		return new NotNull<>(func.apply(t));
	}

	public static <T> NotNull<T> from(T t) throws NotNullException {
		return new NotNull<>(t);
	}
}