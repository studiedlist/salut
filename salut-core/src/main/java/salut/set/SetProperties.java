package salut.set;

import java.util.HashMap;

import salut.notifications.ClientNotifications;
import salut.notifications.PhotographerNotifications;
import salut.workflow.Workflow;
import salut.workflow.WorkflowStep;

public class SetProperties {
	private Visibility visibility;
	private HashMap<WorkflowStep, Boolean> watermark;
	private ClientNotifications clientNotifications;
	private PhotographerNotifications photographerNotifications;
	private Workflow workflow;
}

enum Visibility {
	PUBLIC, BY_LINK, BY_EMAIL;
}