package salut.set;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Id;

import salut.NotEmpty;
import salut.NotNull;
import salut.NotNullList;

public class Set {

	@Id
	private UUID id;

	@Column
	private String name;

	private NotNullList<Photo> photos;

	private SetProperties properties;
	
	public Corrections corrections = new Corrections();

	@SuppressWarnings("unused")
	private Set() {
	}

	public Set(
		NotNull<UUID> id,
		NotEmpty name,
		NotNullList<Photo> photos
	) {
		this.id = id.inner();
		this.name = name.inner();
		this.photos = photos;
	}

	public Set(NotNull<UUID> id, NotEmpty name) {
		this.id = id.inner();
		this.name = name.inner();
		this.photos = new NotNullList<>();
	}

	public void addPhoto(NotNull<Photo> photo) {
		this.photos.add(photo);
	}

	public void addPhotos(NotNullList<Photo> photo) {
		this.photos.addAll(photo);
	}

}
