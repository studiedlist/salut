package salut.set;

import java.util.HashMap;

public class Corrections {
	private final HashMap<Photo, Correction> corrections;
	
	public Corrections(HashMap<Photo, Correction> corrections) {
		this.corrections = corrections;
	}
	
	public Corrections() {
		this.corrections = new HashMap<>();
	}
	
	public HashMap<Photo, Correction> corrections() {
		return this.corrections;
	}
}
