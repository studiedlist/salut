package salut.set;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import salut.NotNull;

@Entity
public class Photo {
	// Will be a part of workflow step
	// private ArrayList<Correction> corrections;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	UUID id;

	@Column
	String title;

	@Column
	String comment;

	@Column
	Boolean photographer_liked;

	@Column
	Boolean client_liked;

	public Photo(NotNull<UUID> id) {
		this.id = id.inner();
	}

	public UUID id() {
		return this.id;
	}

}
