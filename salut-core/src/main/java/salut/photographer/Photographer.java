package salut.photographer;

import java.util.UUID;

import salut.Email;
import salut.NotEmpty;
import salut.NotNull;
import salut.Telegram;


public class Photographer {

	public UUID id;

	public String first_name;

	public String last_name;

	public Email email;

	public Telegram telegramId;

	public Settings settings;

	public String login;

	public Photographer(NotNull<UUID> id) {
		this.id = id.inner();
	}

	// Last name can be set only if first_name is not null
	public Photographer(NotNull<UUID> id, NotEmpty first_name, String last_name) {
		this.id = id.inner();
		this.first_name = first_name.inner();
		this.last_name = last_name;
	}

	public Photographer(UUID id, String first_name, String last_name, Email email, Telegram telegramId, Settings settings, String login) {
		this.id = id;
		this.first_name = first_name;
		this.last_name = last_name;
		this.email = email;
		this.telegramId = telegramId;
		this.settings=settings;
		this.login=login;
	}

	public String first_name() {
		return this.first_name;
	}

	public String last_name() {
		return this.last_name;
	}
}
