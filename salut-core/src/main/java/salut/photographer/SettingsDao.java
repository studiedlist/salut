package salut.photographer;

import salut.*;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "settings")
public class SettingsDao {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "settings_id")
	public UUID id;
	@Column
	public String firstName;
	@Column
	public String lastName;
	@Column
	public String nickName;
	@Column
	public String bio;
	@Column
	public String avatar;
	@Column
	public String telegramUsername;
	@Column
	public Integer telegramId;
	@Column
	public String email;

	@OneToOne
	@MapsId
	@JoinColumn(name = "photographer_id")
	private PhotographerDao photographer;

	public SettingsDao() {
	}

	public SettingsDao(Settings settings) {
		this.id = settings.id;
		this.firstName = settings.firstName;
		this.lastName = settings.lastName;
		this.nickName = settings.nickName.inner();
		this.bio = settings.bio;
		this.avatar = settings.avatar;
		if (settings.telegramId.isUsername()) {
			this.telegramUsername = settings.telegramId.getUsername().inner();
		} else if (settings.telegramId.isId()) {
			this.telegramId = settings.telegramId.getId();
		} else {
			throw new IllegalStateException("Bug");
		}
		this.email = settings.email.inner();
	}

	public Settings into() throws NotNullException, ParsingException {
		Telegram telegram;
		if (this.telegramId != null) {
			telegram = new Telegram(this.telegramId);
		} else if (this.telegramUsername != null) {
			telegram = new Telegram(this.telegramUsername);
		} else {
			throw new NotNullException("Bug");
		}
		Email email = new Email(new NotEmpty(this.email));
		return new Settings(
				this.id, this.firstName, this.lastName, new NotEmpty(this.nickName),
				this.bio, this.avatar, telegram, email
		);
	}
}
