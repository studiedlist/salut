package salut.photographer;
import java.util.UUID;

import salut.Email;
import salut.NotEmpty;
import salut.Telegram;

public class Settings {

	public UUID id;
	public String firstName;
	public String lastName;
	public NotEmpty nickName;
	public String bio;
	public String avatar;
	public Telegram telegramId;
	public Email email;

	public Settings(
		UUID id,
		String firstName,
		String lastName,
		NotEmpty nickName,
		String bio,
		String avatar,
		Telegram telegramId,
		Email email
	) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.nickName = nickName;
		this.bio = bio;
		this.avatar = avatar;
		this.telegramId = telegramId;
		this.email = email;
	}
}
