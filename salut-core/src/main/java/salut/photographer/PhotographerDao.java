package salut.photographer;

import salut.*;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "photographer")
public class PhotographerDao {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "photographer_id")
	public UUID id;

	@Column
	public String first_name;

	@Column
	public String last_name;

	@Column
	public String email;

	@Column
	public String telegramUsername;
	@Column
	public Integer telegramId;

	@OneToOne(mappedBy = "photographer", cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn
	public SettingsDao settings;

	@Column
	public String login;

	public PhotographerDao() {
	}

	public PhotographerDao(Photographer photographer) throws NotNullException {
		this.id = photographer.id;
		this.first_name = photographer.first_name;
		this.last_name = photographer.last_name;
		this.email = photographer.email.inner();
		if (photographer.telegramId.isId()) {
			this.telegramId = photographer.telegramId.getId();
		} else if (photographer.telegramId.isUsername()) {
			this.telegramUsername = photographer.telegramId.getUsername().inner();
		}
		this.settings = new SettingsDao(photographer.settings);
		this.login = photographer.login;
	}

	public Photographer into() throws NotNullException, ParsingException {
		Telegram telegram;
		if (this.telegramId != null) {
			telegram = new Telegram(telegramId);
		} else if (this.telegramUsername != null) {
			telegram = new Telegram(telegramUsername);
		} else {
			throw new NotNullException("Bug");
		}
		Email email = new Email(new NotEmpty(this.email));
		return new Photographer(
				this.id, this.first_name, this.last_name, email, telegram,
				this.settings.into(), this.login
		);
	}
}
