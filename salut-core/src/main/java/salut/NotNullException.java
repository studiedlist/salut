package salut;

@SuppressWarnings("serial")
public class NotNullException extends Exception {

	public NotNullException(String msg) {
		super(msg);
	}

}
