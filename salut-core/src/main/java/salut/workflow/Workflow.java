package salut.workflow;

import java.util.UUID;

import salut.NotEmpty;
import salut.NotNull;

public class Workflow {

	public final UUID id;
	public final String name;
	public final boolean isYanked;

	public Workflow(NotEmpty name) {
		this.id = UUID.randomUUID();
		this.name = name.inner();
		this.isYanked = false;
	}

	public Workflow(NotNull<UUID> id, NotEmpty name, boolean isYanked) {
		this.id = id.inner();
		this.name = name.inner();
		this.isYanked = isYanked;
	}

}
