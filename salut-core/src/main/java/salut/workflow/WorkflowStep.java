package salut.workflow;

public interface WorkflowStep {

	String name();
	String description();

}
