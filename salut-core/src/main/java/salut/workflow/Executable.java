package salut.workflow;

import io.vavr.control.Option;

public interface Executable<A> extends WorkflowStep {

	public void process(A a);

	public static enum ExecutableResult {
		NEXT, REPEAT
	}
	
	public Option<ExecutableResult> result();

}

