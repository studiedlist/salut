package salut.workflow.steps;

import salut.workflow.WorkflowStep;

public class End implements WorkflowStep {

	@Override
	public String name() {
		return "End";
	}

	@Override
	public String description() {
		return "set end";
	}

}