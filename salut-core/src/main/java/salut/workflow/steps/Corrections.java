package salut.workflow.steps;

import io.vavr.control.Option;
import salut.Empty;
import salut.set.Set;
import salut.workflow.Executable;

public class Corrections implements Executable<Empty> {

	private Option<ExecutableResult> result = Option.none();
	private final Set set;

	public Corrections(Set set) {
		this.set = set;
	}

	@Override
	public String name() {
		return "Corrections";
	}

	@Override
	public String description() {
		return "";
	}

	@Override
	public void process(Empty a) {
		if (set.corrections.corrections().size() > 0) {
			result = Option.of(ExecutableResult.REPEAT);
		} else {
			result = Option.of(ExecutableResult.NEXT);
		}
	}

	@Override
	public Option<ExecutableResult> result() {
		var result = this.result;
		this.result = Option.none();
		return result;
	}

}