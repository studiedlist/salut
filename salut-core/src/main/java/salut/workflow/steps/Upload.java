package salut.workflow.steps;

import io.vavr.control.Option;
import salut.NotNull;
import salut.NotNullList;
import salut.set.Photo;
import salut.set.Set;
import salut.workflow.Executable;

public class Upload implements Executable<NotNullList<Photo>> {

	private Set set;
	private boolean isCompleted = false;

	public Upload(NotNull<Set> set) {
		this.set = set.inner();
	}

	@Override
	public void process(NotNullList<Photo> photos) throws IllegalStateException {
		if (this.isCompleted) {
			throw new IllegalStateException(
					"Workflow step cannot be completed twice"
			);
		}
		this.set.addPhotos(photos);
		this.isCompleted = true;
	}

	@Override
	public Option<ExecutableResult> result() {
		return this.isCompleted ? Option.of(ExecutableResult.NEXT) : Option.none();
	}

	@Override
	public String name() {
		return "Upload";
	}

	@Override
	public String description() {
		return "";
	}

}