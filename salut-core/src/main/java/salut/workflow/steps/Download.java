package salut.workflow.steps;

import io.vavr.control.Option;
import salut.Empty;
import salut.workflow.Executable;

public class Download implements Executable<Empty> {

	private boolean isCompleted = false;

	@Override
	public String name() {
		return "Download";
	}

	@Override
	public String description() {
		return "";
	}

	@Override
	public void process(Empty a) {
		this.isCompleted = true;
	}

	@Override
	public Option<ExecutableResult> result() {
		return this.isCompleted ? Option.of(ExecutableResult.NEXT) : Option.none();
	}

}
