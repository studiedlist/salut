package salut.workflow;

import java.util.function.Consumer;

import org.apache.commons.lang3.tuple.Pair;

import io.vavr.control.Either;
import salut.NotNull;
import salut.NotNullException;
import salut.NotNullList;

public abstract class WorkflowSteps {

	private final NotNull<WorkflowVersion> workflow;

	private WorkflowSteps(NotNull<WorkflowVersion> workflow) {
		this.workflow = workflow;
	}

	public abstract WorkflowStep current();

	public abstract int current_index();

	public static WorkflowSteps from(
		NotNull<WorkflowVersion> workflow,
		NotNullList<WorkflowStep> steps
	) throws IllegalArgumentException, NotNullException {
		compare(steps, workflow.inner().steps);
		var pair = validateSteps(steps);
		return new PendingWorkflowSteps(pair.getLeft(), pair.getRight(), workflow);
	}

	private static boolean isExecutable(NotNull<WorkflowStep> s) {
		return s.inner() instanceof Executable;
	}

	private static Pair<NotNullList<Executable<?>>, NotNull<WorkflowStep>> validateSteps(
		NotNullList<WorkflowStep> steps
	) throws IllegalArgumentException, NotNullException {
		var s = steps;
		var last = s.size() - 1;
		NotNullList<Executable<?>> list = new NotNullList<>();
		for (int x = 0; x < s.size(); x++) {
			var elem = s.get(x);
			if (x == last) {
				if (isExecutable(elem)) {
					throw new IllegalArgumentException(
							"Last workflow step cannot be executable"
					);
				}
			} else {
				if (!isExecutable(elem)) {
					throw new IllegalArgumentException(
							"Only last workflow step can be not executable"
					);
				} else {
					list.add(new NotNull<>((Executable<?>) elem.inner()));
				}
			}
		}
		var last_elem = s.get(last);
		return Pair.of(list, last_elem);
	}

	private static void compare(
		final NotNullList<WorkflowStep> steps,
		final NotNullList<Class<? extends WorkflowStep>> classes
	) throws IllegalArgumentException {
		if (steps.size() != classes.size()) {
			throw new IllegalArgumentException("Length mismatch");
		}
		for (int x = 0; x < steps.size(); x++) {
			var step = steps.get(x).inner();
			var clazz = classes.get(x).inner();
			if (step.getClass() != clazz) {
				throw new IllegalArgumentException("Class mismatch");
			}
		}
	}

	public static class PendingWorkflowSteps extends WorkflowSteps {

		private NotNullList<Executable<?>> steps;
		private NotNull<WorkflowStep> last;

		private int current_index;

		private NotNull<Executable<?>> current;

		private PendingWorkflowSteps(
			NotNullList<Executable<?>> steps,
			NotNull<WorkflowStep> last,
			NotNull<WorkflowVersion> workflow
		) {
			super(workflow);
			this.steps = steps;
			this.last = last;
			this.current_index = 0;
			this.current = this.steps.get(current_index);
		}

		private PendingWorkflowSteps(
			PendingWorkflowSteps steps,
			NotNull<Executable<?>> elem,
			NotNull<WorkflowVersion> workflow
		) {
			super(workflow);
			this.steps = steps.steps;
			this.last = steps.last;
			this.current = elem;
			this.current_index = steps.current_index;
		}

		public Executable<?> current() {
			return this.current.inner();
		}

		public void map(Consumer<Executable<?>> func) {
			func.accept(this.current.inner());
		}

		public <B extends Executable<?>> void map(
			Consumer<B> func,
			Class<B> clazz
		) throws ClassCastException {
			B current = clazz.cast(this.current.inner());
			func.accept(current);
		}

		public <B extends Executable<?>> Either<WorkflowSteps, PendingWorkflowSteps> mapAndNext(
			Consumer<B> func,
			Class<B> clazz
		) throws ClassCastException {
			this.map(func, clazz);
			return this.next();
		}

		public Either<WorkflowSteps, PendingWorkflowSteps> next() {
			var res = this.current.inner().result().map((var r) -> {
				Either<WorkflowSteps, PendingWorkflowSteps> e;
				switch (r) {
					case NEXT :
						this.current_index++;
						if (this.current_index < this.steps.size()) {
							var next = steps.get(current_index);
							e = Either
									.left(new PendingWorkflowSteps(this, next, super.workflow));
						} else {
							e = Either
									.left(new CompletedWorkflowSteps(this.last, super.workflow));
						}
						break;
					case REPEAT :
						e = Either.left(this);
						break;
					default :
						throw new IllegalStateException(
								"Bug: not all executable result variants covered"
						);
				};
				return e;
			});
			if (res.isEmpty()) {
				return Either.right(this);
			} else {
				return res.get();
			}
		}

		public int current_index() {
			return this.current_index;
		}
	}

	public static class CompletedWorkflowSteps extends WorkflowSteps {

		private NotNull<WorkflowStep> current;

		public CompletedWorkflowSteps(
			NotNull<WorkflowStep> current,
			NotNull<WorkflowVersion> workflow
		) {
			super(workflow);
			this.current = current;
		}

		public WorkflowStep current() {
			return this.current.inner();
		}

		public int current_index() {
			return super.workflow.inner().steps.size() - 1;
		}
	}
}