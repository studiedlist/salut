package salut.workflow;

public class UnknownStepException extends Exception {
	private static final long serialVersionUID = 1L;

	public UnknownStepException(String msg) {
		super(msg);
	}
}
