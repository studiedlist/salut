package salut.workflow.dao;

import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@IdClass(WorkflowVersionId.class)
public class WorkflowVersionDao {

	@Id
	private UUID workflowId;
	
	@Id
	private Integer number;
	
	@ElementCollection
	@Column
	private List<String> steps;
	
	@ManyToOne
	@JoinColumn(name = "test")
	private WorkflowDao workflow;
	
	private WorkflowVersionDao() {}

}
