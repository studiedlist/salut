package salut.workflow.dao;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import salut.NotEmpty;
import salut.NotNull;
import salut.NotNullException;
import salut.workflow.Workflow;

@Entity
public class WorkflowDao {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;

	@Column(nullable = false)
	private String name;

	@SuppressWarnings("unused")
	private WorkflowDao() {
	}

	public Workflow into() throws NotNullException {
		return new Workflow(NotNull.from(id), new NotEmpty(name), false);
	}

	public WorkflowDao(Workflow workflow) {
		this.id = workflow.id;
		this.name = workflow.name;
	}
}