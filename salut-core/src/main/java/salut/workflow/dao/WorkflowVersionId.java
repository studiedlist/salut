package salut.workflow.dao;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

import org.apache.commons.lang3.builder.EqualsBuilder;

public class WorkflowVersionId implements Serializable {
	private static final long serialVersionUID = 1L;

	public UUID workflowId;
	public Integer number;

	@Override
	public int hashCode() {
		return Objects.hash(this.workflowId, number);
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (o instanceof WorkflowVersionId w) {
			return new EqualsBuilder()
					.append(this.number, w.number)
					.append(this.workflowId, w.workflowId)
					.isEquals();
		} else {
			return false;
		}
	}

}
