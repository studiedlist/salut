package salut.workflow;

import java.util.HashMap;
import java.util.Objects;

import salut.workflow.steps.Corrections;
import salut.workflow.steps.Download;
import salut.workflow.steps.End;
import salut.workflow.steps.Feedback;
import salut.workflow.steps.Filtration;
import salut.workflow.steps.Upload;

public class WorkflowService {

	private static HashMap<Class<? extends WorkflowStep>, String> ids = ids();

	private static HashMap<Class<? extends WorkflowStep>, String> ids() {
		HashMap<Class<? extends WorkflowStep>, String> m = new HashMap<>();
		m.put(Upload.class, "upload");
		m.put(Download.class, "download");
		m.put(Filtration.class, "filtration");
		m.put(End.class, "end");
		m.put(Corrections.class, "corrections");
		m.put(Feedback.class, "feedback;");
		return m;
	}

	public static Class<? extends WorkflowStep> parse(
		String name
	) throws UnknownStepException {
		for (var entry : ids.entrySet()) {
			if (Objects.equals(name, entry.getValue())) {
				return entry.getKey();
			}
		}
		throw new UnknownStepException("Unknown workflow step: " + name);
	}

	public static String idOf(
		Class<? extends WorkflowStep> clazz
	) throws UnknownStepException {
		var id = ids.get(clazz);
		if (id == null) {
			throw new UnknownStepException("Bug: no id for class " + clazz);
		}
		return id;

	}

}
