package salut.workflow;

import salut.NotNull;
import salut.NotNullList;

public class WorkflowVersion {
	public final NotNull<Workflow> workflow;
	public final int number;
	public final NotNullList<Class<? extends WorkflowStep>> steps;

	public WorkflowVersion(
		int number,
		NotNull<Workflow> workflow,
		NotNullList<Class<? extends WorkflowStep>> steps
	) {
		this.workflow = workflow;
		this.number = number;
		this.steps = steps;
	}
}
