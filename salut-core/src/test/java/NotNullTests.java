import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;

import salut.NotEmpty;
import salut.NotNull;
import salut.NotNullException;

class NotNullTests {

	@Test
	void notNullValidatesAndStoresValue() throws NotNullException {
		String empty = null;
		assertThrows(NotNullException.class, () -> new NotNull<String>(empty));

		String s = "test";
		assertEquals(s, new NotNull<String>(s).inner());
	}

	@Test
	void notEmptyValidatesAndStoresValus() throws NotNullException {
		String empty1 = null;
		assertThrows(NotNullException.class, () -> new NotEmpty(empty1));
		String empty2 = "";
		assertThrows(NotNullException.class, () -> new NotEmpty(empty2));
		String empty3 = " ";
		assertThrows(NotNullException.class, () -> new NotEmpty(empty3));

		String s = "test";
		assertEquals(s, new NotEmpty(s).inner());
	}

}
