import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import salut.Email;
import salut.NotNullException;
import salut.ParsingException;

public class EmailTests {

	@Test
	void testEmail() throws NotNullException, ParsingException {
		assertThrows(ParsingException.class, () -> new Email("abc"));
		assertThrows(ParsingException.class, () -> new Email("abc@"));
		assertThrows(ParsingException.class, () -> new Email("@abc"));
		assertThrows(ParsingException.class, () -> new Email("abc@cde"));
		new Email("abc@gmail.com");
	}
}
