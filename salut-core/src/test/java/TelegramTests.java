import org.junit.jupiter.api.Test;

import salut.NotEmpty;
import salut.NotNullException;
import salut.Telegram;

import static org.junit.jupiter.api.Assertions.*;

public class TelegramTests {
	@Test
	void testTelegram() throws NotNullException {
		assertTrue(new Telegram(0).isId());
		assertTrue(new Telegram("abc").isUsername());
		assertTrue(new Telegram(new NotEmpty("abc")).isUsername());

		assertNotNull(new Telegram(0).getId());
		assertNull(new Telegram(0).getUsername());

		assertNotNull(new Telegram("abc").getUsername());
		assertNull(new Telegram("abc").getId());
	}
}
