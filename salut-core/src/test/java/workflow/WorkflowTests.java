package workflow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.function.Function;

import org.junit.jupiter.api.Test;

import io.vavr.control.Option;
import salut.Empty;
import salut.NotEmpty;
import salut.NotNull;
import salut.NotNullException;
import salut.NotNullList;
import salut.workflow.Executable;
import salut.workflow.Workflow;
import salut.workflow.WorkflowStep;
import salut.workflow.WorkflowSteps;
import salut.workflow.WorkflowSteps.CompletedWorkflowSteps;
import salut.workflow.WorkflowSteps.PendingWorkflowSteps;
import salut.workflow.WorkflowVersion;

class WorkflowTests {

	@Test
	void workflowStepsValidation() throws NotNullException {
		NotNullList<Class<? extends WorkflowStep>> s;
		NotNullList<WorkflowStep> list;
		var workflow = NotNull.from(new Workflow(new NotEmpty("test")));
		NotNull<WorkflowVersion> version;
		// Test if WorkflowSteps created successfuly
		{
			s = new NotNullList<>();
			s.add(UploadStep.class);
			s.add(DownloadStep.class);
			list = new NotNullList<>(new UploadStep(), new DownloadStep());
			version = NotNull.from(new WorkflowVersion(0, workflow, s));

			WorkflowSteps.from(version, list);
		}

		// Test if WorkflowSteps handles classes and steps mismatch
		{
			s = new NotNullList<>();
			s.add(DownloadStep.class);
			s.add(DownloadStep.class);
			list = new NotNullList<>(new UploadStep(), new DownloadStep());

			version = NotNull.from(new WorkflowVersion(0, workflow, s));

			final var l = list;
			final var w = version;

			assertThrows(
					IllegalArgumentException.class, () -> WorkflowSteps.from(w, l)
			);
		}

		// Test if WorkflowSteps handles last executable step
		{
			list = new NotNullList<>(new DownloadStep(), new UploadStep());

			s = new NotNullList<>();
			s.add(DownloadStep.class);
			s.add(UploadStep.class);

			version = NotNull.from(new WorkflowVersion(0, workflow, s));

			final var l = list;
			final var w = version;

			assertThrows(
					IllegalArgumentException.class, () -> WorkflowSteps.from(w, l)
			);
		}
	}

	@Test
	void workflowStepsTraversal() throws NotNullException {
		NotNullList<Class<? extends WorkflowStep>> st = new NotNullList<>();
		st.add(UploadStep.class);
		st.add(FiltrationStep.class);
		st.add(DownloadStep.class);
		final var workflow = NotNull.from(new Workflow(new NotEmpty("test")));
		final var version = NotNull.from(new WorkflowVersion(0, workflow, st));
		NotNullList<WorkflowStep> list = new NotNullList<>(
				new UploadStep(), new FiltrationStep(), new DownloadStep()
		);
		Function<WorkflowSteps, PendingWorkflowSteps> match_steps = (steps) -> {
			if (steps instanceof PendingWorkflowSteps s) {
				return s;
			} else {
				throw new IllegalStateException("");
			}
		};
		WorkflowSteps workflowSteps = WorkflowSteps.from(version, list);
		PendingWorkflowSteps pending = match_steps.apply(workflowSteps);

		assertTrue(pending.next().isRight());
		final var p = pending;
		assertThrows(ClassCastException.class, () -> p.map((FiltrationStep s) -> {
		}, FiltrationStep.class));
		var either = pending
				.mapAndNext((UploadStep s) -> s.process(new Empty()), UploadStep.class);
		assertTrue(either.isLeft());

		workflowSteps = either.getLeft();
		pending = match_steps.apply(workflowSteps);

		assertTrue(pending.next().isRight());
		final var v = pending;
		assertThrows(ClassCastException.class, () -> v.map((UploadStep s) -> {
		}, UploadStep.class));
		either = pending.mapAndNext(
				(FiltrationStep s) -> s.process(new Empty()), FiltrationStep.class
		);
		assertTrue(either.isLeft());

		workflowSteps = either.getLeft();
		pending = match_steps.apply(workflowSteps);

		assertTrue(pending.next().isRight());
		either = pending.mapAndNext(
				(FiltrationStep s) -> s.process(new Empty()), FiltrationStep.class
		);

		workflowSteps = either.getLeft();
		assertTrue(either.isLeft());
		if (workflowSteps instanceof CompletedWorkflowSteps s) {
			assertTrue(s.current() instanceof DownloadStep);
		} else {
			throw new IllegalStateException("");
		}
	}

	class TestStep implements WorkflowStep {
		@Override
		public String name() {
			return "test";
		}

		@Override
		public String description() {
			return "test";
		}
	}
	abstract class ExecutableTestStep extends TestStep
			implements
				Executable<Empty> {

		private boolean isCompleted = false;

		@Override
		public void process(Empty empty) {
			this.isCompleted = true;
		}

		@Override
		public Option<ExecutableResult> result() {
			return this.isCompleted
					? Option.of(ExecutableResult.NEXT)
					: Option.none();
		}
	}

	class UploadStep extends ExecutableTestStep {
	}
	class FiltrationStep extends ExecutableTestStep {

		private int callCount = 0;

		@Override
		public Option<ExecutableResult> result() {
			if (!super.isCompleted) {
				return Option.none();
			}
			if (this.callCount < 1) {
				this.callCount++;
				super.isCompleted = false;
				return Option.some(ExecutableResult.REPEAT);
			} else {
				return Option.some(ExecutableResult.NEXT);
			}
		}
	}
	class DownloadStep extends TestStep {
	}
}
