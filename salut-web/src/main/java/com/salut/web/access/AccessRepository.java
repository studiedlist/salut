package com.salut.web.access;

import org.springframework.data.jpa.repository.JpaRepository;

import salut.access.UserCredentials;

public interface AccessRepository
		extends
			JpaRepository<UserCredentials, String> {
	UserCredentials findByLogin(String login);
}
