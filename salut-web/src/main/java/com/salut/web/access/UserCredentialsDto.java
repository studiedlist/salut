package com.salut.web.access;

import java.util.UUID;

import org.springframework.security.crypto.password.PasswordEncoder;

import salut.NotEmpty;
import salut.NotNull;
import salut.NotNullException;
import salut.access.UserCredentials;

public class UserCredentialsDto {

	public NotEmpty login;
	public NotEmpty password;

	public void setLogin(String login) throws NotNullException {
		this.login = new NotEmpty(login);
	}

	public void setPassword(String password) throws NotNullException {
		this.password = new NotEmpty(password);
	}

	public UserCredentials enrich(UUID id, PasswordEncoder encoder) {
		var password = encoder.encode(this.password.inner());
		try {
			return new UserCredentials(
					new NotNull<>(id), this.login, new NotEmpty(password)
			);
		} catch (NotNullException e) {
			throw new NullPointerException(e.getMessage());
		}
	}
}
