package com.salut.web.access;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import salut.access.UserCredentials;

@Service
public class AccessService implements UserDetailsService {

	@Autowired
	private AccessRepository repository;

	@Override
	public UserDetails loadUserByUsername(String username) {
		UserCredentials creds = this.get(username);
		if (creds == null) {
			throw new UsernameNotFoundException(username);
		}
		return new UserPrincipal(creds);
	}

	public UserCredentials get(String login) {
		return repository.findByLogin(login);
	}

	public void add(UserCredentials creds) {
		repository.save(creds);
	}

	public List<UserCredentials> list() {
		return repository.findAll();
	}

}
