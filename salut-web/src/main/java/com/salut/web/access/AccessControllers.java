package com.salut.web.access;

import java.net.URI;
import java.security.Principal;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.salut.web.view.Template;

@RestController
public class AccessControllers {

	@Autowired
	private AccessService service;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@PostMapping("/register")
	ResponseEntity<String> register_post(
		@ModelAttribute UserCredentialsDto creds
	) {
		service.add(creds.enrich(UUID.randomUUID(), passwordEncoder));
		return ResponseEntity.status(HttpStatus.SEE_OTHER)
				.location(URI.create("/login")).body("");
	}

	@GetMapping("/register")
	ModelAndView register_get(Principal principal) {
		return new Template("access/register", principal);
	}

	@GetMapping("/login")
	ModelAndView login(Principal principal, Map<String, Object> model) {
		return new Template("access/login", principal);
	}

}