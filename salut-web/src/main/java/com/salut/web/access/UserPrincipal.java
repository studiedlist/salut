package com.salut.web.access;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import salut.access.UserCredentials;

@SuppressWarnings("serial")
public class UserPrincipal implements UserDetails {

	private UserCredentials userCredentials;

	public UserPrincipal(UserCredentials userCredentials) {
		this.userCredentials = userCredentials;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public String getPassword() {
		return userCredentials.password();
	}

	@Override
	public String getUsername() {
		return userCredentials.login();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
