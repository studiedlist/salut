package com.salut.web.access;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import salut.access.UserCredentials;

@Component
public class UserCredentialsAuthenticationProvider
		implements
			AuthenticationProvider {

	@Autowired
	private AccessService accessService;

	@Autowired
	private PasswordEncoder encoder;

	@Override
	public Authentication authenticate(
		Authentication authentication
	) throws AuthenticationException {
		String username = authentication.getName();
		String password = authentication.getCredentials().toString();

		UserCredentials creds = accessService.get(username);
		if (creds == null) {
			throw new BadCredentialsException("Details not found");
		}

		if (encoder.matches(password, creds.password())) {
			var token = new UsernamePasswordAuthenticationToken(
					username, password, new ArrayList<>()
			);
			return token;
		} else {
			throw new BadCredentialsException("Password mismatch");
		}
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}
