package com.salut.web.view;

import java.security.Principal;
import java.util.Map;

import org.springframework.web.servlet.ModelAndView;

// Template class is designed to guarantee that each rendered
// view will explicitly be provided with Principal
public class Template extends ModelAndView {

	public Template(String path, Principal principal) {
		super(path);
		var model = super.getModel();
		if (principal != null) {
			model.put("login", principal.getName());
		}
	}

	public Template(String path, Map<String, Object> model, Principal principal) {
		super(path, model);
		if (principal != null) {
			model.put("login", principal.getName());
		}
	}

}
