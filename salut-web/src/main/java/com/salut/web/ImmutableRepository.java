package com.salut.web;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

@NoRepositoryBean
public interface ImmutableRepository<V, K> extends Repository<V, K> {

	void save(V v);
	V getById(K id);
	void removeById(K id);
}
