package com.salut.web;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.io.ClassPathTemplateLoader;
import com.github.jknack.handlebars.springmvc.HandlebarsViewResolver;

@EntityScan(basePackages = "salut")
@EnableJpaRepositories
@SpringBootConfiguration
public class ApplicationConfig implements WebMvcConfigurer {

	private static HandlebarsViewResolver handlebars = null;

	@Bean
	HandlebarsViewResolver getViewResolver(ResourceLoader resourceLoader) {
		ClassPathTemplateLoader loader = new ClassPathTemplateLoader(
				"/templates", ".hbs"
		);
		Handlebars hbs = new Handlebars(loader);
		handlebars = new HandlebarsViewResolver(hbs);
		// Setting to true may cause problems with updates
		handlebars.setCache(false);
		return handlebars;
	}
	@Bean
	DataSource dataSource() {
		EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
		return builder.setType(EmbeddedDatabaseType.H2).build();
	}

	@Override
	public void extendHandlerExceptionResolvers(
		List<HandlerExceptionResolver> resolvers
	) {
		resolvers.clear();
		resolvers.add(new ExceptionResolver());
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/css/**")
				.addResourceLocations("classpath:static/css/");
		registry.addResourceHandler("/img/**")
				.addResourceLocations("classpath:static/img/");
		registry.addResourceHandler("/js/**")
				.addResourceLocations("classpath:static/js/");
		registry.addResourceHandler("/fonts/**")
				.addResourceLocations("classpath:static/fonts/");
	}
}
