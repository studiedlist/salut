package com.salut.web;

import java.security.Principal;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.salut.web.view.Template;

@RestController
public class Controllers {

	@GetMapping("/")
	ModelAndView index(Principal principal) {
		return new Template("index", principal);
	}

	@GetMapping("/features")
	ModelAndView features(Principal principal) {
		return new Template("features", principal);
	}

}
