package com.salut.web.photographer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import salut.NotNullException;
import salut.ParsingException;
import salut.photographer.Settings;

import java.util.UUID;

@Service
public class SettingsService {

	@Autowired
	private SettingsRepository repository;

	public Settings get(UUID id) throws ParsingException, NotNullException {
		return repository.findById(id).get().into();
	}

}