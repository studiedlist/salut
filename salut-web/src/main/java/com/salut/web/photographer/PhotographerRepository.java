package com.salut.web.photographer;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import salut.photographer.PhotographerDao;

public interface PhotographerRepository extends JpaRepository<PhotographerDao, UUID> {
    PhotographerDao findByLogin(String login);
}
