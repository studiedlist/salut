package com.salut.web.photographer;

import com.salut.web.view.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import salut.photographer.Photographer;

import java.security.Principal;
import java.util.UUID;

@RestController
@RequestMapping("/settings")
public class SettingsControllers {

	@Autowired
	private SettingsService settingsService;

	@GetMapping("")
	public ModelAndView getSettings(Principal principal) {
		return new Template("profile/settings", principal);
	}

	@PutMapping("/account")
	public ModelAndView updateAccount(Principal principal) {

		return new Template("profile/settings", principal);
	}

}
