package com.salut.web.photographer;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import salut.photographer.SettingsDao;

public interface SettingsRepository extends JpaRepository<SettingsDao, UUID> {
}
