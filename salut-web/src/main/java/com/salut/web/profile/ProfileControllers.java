package com.salut.web.profile;

import java.security.Principal;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.salut.web.view.Template;

@RestController
public class ProfileControllers {

	@GetMapping("/me")
	ModelAndView me(Principal principal) {
		return new Template("profile/me", principal);
	}
}
