package com.salut.web.workflow;

import org.springframework.data.repository.Repository;

import salut.workflow.dao.WorkflowVersionDao;
import salut.workflow.dao.WorkflowVersionId;

public interface WorkflowVersionRepository
		extends
			Repository<WorkflowVersionDao, WorkflowVersionId> {
	void save(WorkflowVersionDao dto);
	WorkflowVersionDao getByWorkflowId(WorkflowVersionId id);
	void removeByWorkflowId(WorkflowVersionId id);
}
