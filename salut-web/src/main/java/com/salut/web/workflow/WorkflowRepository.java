package com.salut.web.workflow;

import java.util.UUID;

import com.salut.web.ImmutableRepository;

import salut.workflow.dao.WorkflowDao;

public interface WorkflowRepository
		extends
			ImmutableRepository<WorkflowDao, UUID> {
}
