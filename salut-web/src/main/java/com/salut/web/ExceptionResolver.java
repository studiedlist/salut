package com.salut.web;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;

import com.salut.web.view.Template;

@Component
@ControllerAdvice
public class ExceptionResolver extends DefaultHandlerExceptionResolver {

	Logger log = LoggerFactory.getLogger(ExceptionResolver.class);

	@Override
	protected ModelAndView doResolveException(
		HttpServletRequest request,
		HttpServletResponse response,
		Object handler,
		Exception ex
	) {
		log.error("Exception occured:\n" + ex);
		Principal principal = SecurityContextHolder.getContext()
				.getAuthentication();
		Map<String, Object> model = new HashMap<>();
		model.put("msg", ex.getMessage());
		return new Template("error", model, principal);
	}

	@Override
	protected ModelAndView handleNoHandlerFoundException(
		NoHandlerFoundException ex,
		HttpServletRequest request,
		HttpServletResponse response,
		@Nullable Object handler
	) {
		return this.doResolveException(request, response, handler, ex);
	}

}