package com.salut.web.set;

import java.security.Principal;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.salut.web.view.Template;

@RestController
@RequestMapping("/set")
public class SetControllers {

	@GetMapping("/new")
	ModelAndView newSet(Principal principal) {
		return new Template("set/new", principal);
	}

	@GetMapping("/create")
	ModelAndView createSet(Principal principal) {
		return new Template("set/create", principal);
	}

	@GetMapping("/set/info")
	ModelAndView gallery(Principal principal) {
		return new Template("set/info", principal);
	}
}
