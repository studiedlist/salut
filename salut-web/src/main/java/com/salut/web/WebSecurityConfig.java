package com.salut.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import com.salut.web.access.AccessService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {

	@Bean
	SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable()
				.authorizeHttpRequests(
						requests -> requests.antMatchers("/").permitAll()
								.antMatchers("/css/**").permitAll().antMatchers("/js/**")
								.permitAll().antMatchers("/img/**").permitAll()
								.antMatchers("/fonts/**").permitAll().antMatchers("/features")
								.permitAll().antMatchers("/login").permitAll()
								.antMatchers("/logout").permitAll().antMatchers("/register")
								.permitAll().anyRequest().authenticated()
				)
				.formLogin(
						(form) -> form.loginPage("/login").usernameParameter("login")
								.permitAll()
				).logout((logout) -> logout.permitAll());

		return http.build();
	}

	private AccessService accessService = new AccessService();

	@Bean
	UserDetailsService userDetailsService() {
		return accessService;
	}

	private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	@Bean
	PasswordEncoder passwordEncoder() {
		return encoder;
	}
}
